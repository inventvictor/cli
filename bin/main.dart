import 'dart:io';
import 'dart:async';
import 'dart:convert' show utf8, json;

import 'package:cli/aws_file_upload.dart';
import 'package:cli/invoice_pdf_page.dart';
import 'package:cli/models/models.dart';
import 'package:cli/multiple_pdf_page.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:image/image.dart' as img;
import 'package:dotenv/dotenv.dart' show load, env;
import 'package:date_format/date_format.dart';

// import 'package:args/args.dart';
// import 'package:args/command_runner.dart';
// import 'package:cli/cli.dart' as cli;
// import 'package:cli/command.dart';

// void main(List<String> arguments) {

//   HttpServer.bind(InternetAddress.loopbackIPv4, 8080)
//       .then((HttpServer server) {
//     print('listening on localhost, port ${server.port}');
//     server.listen((HttpRequest request) {
//       //request.response.write('Hello, world!');
//       //request.response.close();
//       if (request.method == 'GET') {
//         handleGet(request);
//       } else {
//         request.response.statusCode = HttpStatus.methodNotAllowed;
//         request.response.write('Unsupported request: ${request.method}.');
//         request.response.close();
//       }
//     });
//   }).catchError((e) => print(e.toString()));
  
//   // var runner = CommandRunner('ems', 'Energy Management System')
//   //     ..addCommand(ReportGenCommand());
//   //     //..run(['commit', '-a']); 
  
//   // runner.run(arguments).catchError((error) {
//   //   if (error is! UsageException) throw error;
//   //   print(error);
//   //   exit(64); // Exit code 64 indicates a usage error.
//   // });
  
// }

Future main() async {
  // var dateTime = DateTime.fromMillisecondsSinceEpoch((1583155405313));
  // print (formatDate(dateTime, [MM, ' ', d, ', ', yyyy, ' ', HH, ':', mm]));
  load();
  var server = await HttpServer.bind('0.0.0.0', 8080);
  print('Listening on http://${server.address.address}:${server.port}/');
  await listenForRequests(server);
}

Future listenForRequests(HttpServer requests) async {
  await for (HttpRequest request in requests) {
    switch (request.method) {
      case 'POST':
        await handlePost(request);
        break;
      default:
        defaultHandler(request);
        break;
    }
  }
  print('No more requests.');
}

Future handlePost(HttpRequest request) async {
  Map<dynamic, dynamic> decoded;
  String sCode;
  int t;

  addCorsHeaders(request.response);

  try {
    decoded = await utf8.decoder.bind(request).transform(json.decoder).first as Map<dynamic, dynamic>;
  } catch (e) {
    print('Request listen error: $e');
    return;
  }

  try{
    if (decoded.containsKey('data')) {
      ReportModel reportModel;
      OrgNModel nModel;
      String docType;
      List<String> charts;

      charts = List();
      reportModel = getReportData(json.encode(decoded['data']));
      docType = reportModel.docTypeModel.doc_type;
      nModel = OrgNModel(reportModel.org.n['users'], reportModel.org.n['spaces'], reportModel.org.n['sites']);

      if(docType.toLowerCase() == 'invoice'){
        int i;
        for(i = 0; i < reportModel.graph.data.length; i++){
          GraphDataModel graphDataModel;
          GraphCompareToModel graphCompareToModel;
          graphDataModel = GraphDataModel(reportModel.graph.data[i]['data']['label'], reportModel.graph.data[i]['data']['x'], reportModel.graph.data[i]['data']['y'], reportModel.graph.data[i]['data']['compare_to']);
          graphCompareToModel = GraphCompareToModel(reportModel.graph.data[i]['data']['compare_to']['label'], reportModel.graph.data[i]['data']['compare_to']['y']);
          String chart;
          chart = graphCompareToModel.label.isEmpty ? generateQuickChartUrl(graphDataModel.label, graphDataModel.x, graphDataModel.y) : generateCompareQuickChartUrl(graphDataModel.label, graphCompareToModel.label, graphDataModel.x, graphDataModel.y, graphCompareToModel.y);
          charts.add(chart);
        }
        t = DateTime.now().millisecondsSinceEpoch;

        await generateInvoicePDF(reportModel.org.logo, charts, reportModel.org.name, reportModel.org.email, nModel.users.toString(), nModel.sites.toString(), nModel.spaces.toString(), '${formatDate(DateTime.fromMillisecondsSinceEpoch(reportModel.from), [MM, ' ', d, ', ', yyyy])} - ${formatDate(DateTime.fromMillisecondsSinceEpoch(reportModel.to), [MM, ' ', d, ', ', yyyy])}', reportModel.site.name, reportModel.site.device, reportModel.site.addr, t, reportModel.invoiceModel.curr, reportModel.invoiceModel.value, reportModel.invoiceModel.extraCharges, reportModel.invoiceModel.unit, reportModel.invoiceModel.energyUsed, reportModel.invoiceModel.averagePeak, reportModel.invoiceModel.tariff);
        print('Uploading to Amazon S3 ....');
        sCode = await awsUpload(reportModel.site.device.replaceAll(RegExp(r'[^A-Za-z0-9]'),''), t, env['AWSRegion'], env['AWSAccessKeyId'], env['AWSSecretKey'], env['AWSService'], 'https://inventech-etrack.s3.us-east-2.amazonaws.com');
        print('Deleting created folder ...');
        await Directory('${env['PATH']}/$t').delete(recursive: true);
        print('Response sent');
        if(sCode == '200'){
          request.response.statusCode = HttpStatus.ok;
          request.response..write(
            json.encode(
              {
                'error_code': '00',
                'reportlink': 'https://inventech-etrack.s3.us-east-2.amazonaws.com/reports/${getCoolPDFName(reportModel.site.device.replaceAll(RegExp(r'[^A-Za-z0-9]'),''), t)}.pdf',
                'created_at': DateTime.fromMillisecondsSinceEpoch(t).toString()
              }
            )
          );
        }
        else{
          request.response.statusCode = HttpStatus.internalServerError;
          request.response..write(
            json.encode(
              {
                'error_code': '02',
                'reportlink': '',
                'created_at': ''
              }
            )
          );
        }
      }

      else if(docType.toLowerCase() == 'report'){
        int i;
        for(i = 0; i < reportModel.graph.data.length; i++){
          GraphDataModel graphDataModel;
          GraphCompareToModel graphCompareToModel;
          graphDataModel = GraphDataModel(reportModel.graph.data[i]['data']['label'], reportModel.graph.data[i]['data']['x'], reportModel.graph.data[i]['data']['y'], reportModel.graph.data[i]['data']['compare_to']);
          graphCompareToModel = GraphCompareToModel(reportModel.graph.data[i]['data']['compare_to']['label'], reportModel.graph.data[i]['data']['compare_to']['y']);
          String chart;
          chart = graphCompareToModel.label.isEmpty ? generateQuickChartUrl(graphDataModel.label, graphDataModel.x, graphDataModel.y) : generateCompareQuickChartUrl(graphDataModel.label, graphCompareToModel.label, graphDataModel.x, graphDataModel.y, graphCompareToModel.y);
          charts.add(chart);
        }
        
        t = DateTime.now().millisecondsSinceEpoch;
        //t = 1582624117819;
        await generateReportPDF(reportModel.org.logo, charts, reportModel.org.name, reportModel.org.email, nModel.users.toString(), nModel.sites.toString(), nModel.spaces.toString(), '${formatDate(DateTime.fromMillisecondsSinceEpoch(reportModel.from), [MM, ' ', d, ', ', yyyy])} - ${formatDate(DateTime.fromMillisecondsSinceEpoch(reportModel.to), [MM, ' ', d, ', ', yyyy])}', reportModel.site.name, reportModel.site.device, reportModel.site.addr, t);
        print('Uploading to Amazon S3 ....');
        sCode = await awsUpload(reportModel.site.device.replaceAll(RegExp(r'[^A-Za-z0-9]'),''), t, env['AWSRegion'], env['AWSAccessKeyId'], env['AWSSecretKey'], env['AWSService'], 'https://inventech-etrack.s3.us-east-2.amazonaws.com');
        print('Deleting created folder ...');
        await Directory('${env['PATH']}/$t').delete(recursive: true);
        print('Response sent');
        if(sCode == '200'){
          request.response.statusCode = HttpStatus.ok;
          request.response..write(
            json.encode(
              {
                'error_code': '00',
                'reportlink': 'https://inventech-etrack.s3.us-east-2.amazonaws.com/reports/${getCoolPDFName(reportModel.site.device.replaceAll(RegExp(r'[^A-Za-z0-9]'),''), t)}.pdf',
                'created_at': DateTime.fromMillisecondsSinceEpoch(t).toString()
              }
            )
          );
        }
        else{
          request.response.statusCode = HttpStatus.internalServerError;
          request.response..write(
            json.encode(
              {
                'error_code': '02',
                'reportlink': '',
                'created_at': ''
              }
            )
          );
        }
      }

      else{
        request.response.statusCode = HttpStatus.badRequest;
        request.response..write(
          json.encode(
            {
              'error_code': '01',
              'reportlink': '',
              'created_at': ''
            }
          )
        );
      }

      await request.response.close();

    } else {
      request.response.statusCode = HttpStatus.badRequest;
      request.response..write(
        json.encode(
          {
            'error_code': '01',
            'reportlink': '',
            'created_at': ''
          }
        )
      );
      await request.response.close();
    }
  } catch(err){
    print ('catch $err');
    print ('Internal Server Error');
    request.response.statusCode = HttpStatus.internalServerError;
    request.response..write(
      json.encode(
        {
          'error_code': '02',
          'reportlink': '',
          'created_at': ''
        }
      )
    );
    await request.response.close();
  }
  
}

void defaultHandler(HttpRequest request) {
  final response = request.response;
  addCorsHeaders(response);
  response
    ..statusCode = HttpStatus.notFound
    ..write('Not found: ${request.method}, ${request.uri.path}')
    ..close();
}

void addCorsHeaders(HttpResponse response) {
  response.headers.add('Access-Control-Allow-Origin', '*');
  response.headers.add('Access-Control-Allow-Methods', 'POST, OPTIONS');
  response.headers.add('Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept');
}

ReportModel getReportData(String mapi){
  Map<dynamic, dynamic> map = json.decode(mapi);
  ReportModel reportModel;
  reportModel = ReportModel(DocTypeModel.fromJson(map), OrgModel.fromJson(map), SiteModel.fromJson(map), map['from'], map['to'], map['space_name'], InvoiceModel.fromJson(map), GraphModel.fromJson(map));
  return reportModel;
}

String generateQuickChartUrl(String chart1Label, List<dynamic> chartXAxis, List<dynamic> chart1YAxis){
  String url;
  url = 'https://quickchart.io/chart?bkg=white&f=png&c=';
  Map<dynamic, dynamic> map;
  map = {
    'type': 'bar',
    'data': {
      'labels': chartXAxis,
      'datasets': [{
        'label': chart1Label,
        'data': chart1YAxis
      }]
    }
  };
  return url+Uri.encodeFull(json.encode(map));
}

String generateCompareQuickChartUrl(String chart1Label, String chart2Label, List<dynamic> chartXAxis, List<dynamic> chart1YAxis, List<dynamic> chart2YAxis){
  String url;
  url = 'https://quickchart.io/chart?bkg=white&f=png&c=';
  Map<dynamic, dynamic> map;
  map = {
    'type': 'bar',
    'data': {
      'labels': chartXAxis,
      'datasets': [{
        'label': chart1Label,
        'data': chart1YAxis
      },{
        'label': chart2Label,
        'data': chart2YAxis
      }]
    }
  };
  return url+Uri.encodeFull(json.encode(map));
}


void generateReportPDF(String org_logo, List<String> charts, String org_name, String org_email, 
  String org_users, String org_sites, String org_spaces, String from_to, String site_name, 
  String device_name, String site_address, int t) async{
  Document pdf;
  MultiplePagePdf singlePagePdf;
  pdf = Document();
  singlePagePdf = MultiplePagePdf(pdf);
  await singlePagePdf.pdfImageFromOrgLogo(pdf.document, org_logo, t).then((orgLogo) async{
    await singlePagePdf.pdfImageFromEtrackLogo(pdf.document, 'https://etrack.africa/assets/images/etrack/etrack-logo__green.png', t).then((etrackLogo) async{
      await singlePagePdf.getPdfImagesfromCharts(pdf.document, charts, t).then((pdfImages) async{
        await singlePagePdf.generatePDF(orgLogo, etrackLogo, pdfImages, org_name, org_email, org_users, org_sites, org_spaces, from_to, site_name, device_name, site_address, t).whenComplete(() async{
         // print(orgLogo.image.buffer.asUint8List());
        });
      });
    });
  });
}

void generateInvoicePDF(String org_logo, List<String> charts, String org_name, String org_email, 
  String org_users, String org_sites, String org_spaces, String from_to, String site_name, 
  String device_name, String site_address, int t, String curr, double value, double extraCharges, 
  String unit, double energyUsed, double averagePeak, double tariff) async{
  Document pdf;
  InvoicePagePdf invoicePagePdf;
  pdf = Document();
  invoicePagePdf = InvoicePagePdf(pdf);
  await invoicePagePdf.pdfImageFromOrgLogo(pdf.document, org_logo, t).then((orgLogo) async{
    await invoicePagePdf.pdfImageFromEtrackLogo(pdf.document, 'https://etrack.africa/assets/images/etrack/etrack-logo__green.png', t).then((etrackLogo) async{
      await invoicePagePdf.getPdfImagesfromCharts(pdf.document, charts, t).then((pdfImages) async{
        await invoicePagePdf.generateInvoicePDF(orgLogo, etrackLogo, pdfImages, org_name, org_email, org_users, org_sites, org_spaces, from_to, site_name, device_name, site_address, t, curr, value, extraCharges, unit, energyUsed, averagePeak, tariff).whenComplete(() async{
         // print(orgLogo.image.buffer.asUint8List());
        });
      });
    });
  });
}

Future<String> awsUpload(String orgName, int dateTime, String region, String accessKey, String secretKey, String service, String endpointUrl) async{
  AWSFileUpload awsFileUpload;
  String sCode;
  awsFileUpload = AWSFileUpload(region, accessKey, secretKey, service, endpointUrl, dateTime, orgName);
  sCode = await awsFileUpload.uploadFile('/reports/${getCoolPDFName(orgName, dateTime)}.pdf', File('${env['PATH']}/${dateTime}/${dateTime}.pdf'), 'application/pdf', Permissions.public);
  print('Response Status Code: $sCode');
  return sCode;
}

String getCoolPDFName(String name, int dateTime){
  var d = DateTime.fromMillisecondsSinceEpoch(dateTime);
  return ('${name}'+'_'+'${d.day}'+'${d.month}'+'${d.year}'+'${d.hour}'+'${d.minute}'+'${d.second}');
}

// void createRequestFolder(int dateTime, int chartLength){
//   final image = img.Image(64, 64);
//   image.fill(img.getColor(100, 200, 255));

//   //File('/${dateTime}/org_logo.png')..createSync(recursive: true);
//   File('/${dateTime}/etrack_logo.png')..createSync(recursive: true);
//   File('/${dateTime}/chart.png')..createSync(recursive: true);

//   // Encode the image to PNG
//   //final png = img.PngEncoder().encodeImage(image);
//   //File('/org_logo.png').copy('/${dateTime}/org_logo.png');
//   File('/etrack_logo.png').copy('/${dateTime}/etrack_logo.png');
//   File('/chart.png').copy('/${dateTime}/chart.png');
  
//   // File('/${dateTime}/etrack_logo.png')
//   //   ..createSync(recursive: true)
//   //   ..writeAsBytesSync(png);
  
//   // File('/${dateTime}/chart.png')
//   //   ..createSync(recursive: true)
//   //   ..writeAsBytesSync(png);
    
// }

// void copyFilesIntoFolder(int dateTime){

// }
