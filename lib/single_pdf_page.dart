import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:image/image.dart' as img;

class SinglePagePdf{
  Document pdf;

  SinglePagePdf(Document pdf){
    this.pdf = pdf;
  }

  Future<Null> generatePDF(PdfImage org_logo, PdfImage etrack_logo, PdfImage chart) async{
    final Uint8List fontDataBlack = File('assets/fonts/Raleway-Black.ttf').readAsBytesSync();
    final Uint8List fontDataBold = File('assets/fonts/Raleway-Bold.ttf').readAsBytesSync();
    final Uint8List fontDataMedium = File('assets/fonts/Raleway-Medium.ttf').readAsBytesSync();
    final Uint8List fontDataRegular = File('assets/fonts/Raleway-Regular.ttf').readAsBytesSync();
    final Uint8List fontDataLight = File('assets/fonts/Raleway-Light.ttf').readAsBytesSync();
    
    final RalewayBlackTTF = Font.ttf(fontDataBlack.buffer.asByteData());
    final RalewayBoldTTF = Font.ttf(fontDataBold.buffer.asByteData());
    final RalewayMediumTTF = Font.ttf(fontDataMedium.buffer.asByteData());
    final RalewayRegularTTF = Font.ttf(fontDataRegular.buffer.asByteData());
    final RalewayLightTTF = Font.ttf(fontDataLight.buffer.asByteData());

    pdf.addPage(MultiPage(
      pageFormat:
          PdfPageFormat.a4.copyWith(marginBottom: 0.0 * PdfPageFormat.cm, marginTop: 0.5 * PdfPageFormat.cm, marginLeft: 0.0 * PdfPageFormat.cm, marginRight: 0.0 * PdfPageFormat.cm),
      crossAxisAlignment: CrossAxisAlignment.start,
      header: (Context context) {
        if (context.pageNumber == 1) {
          return null;
        }
        return Container(
            alignment: Alignment.centerRight,
            margin: const EdgeInsets.only(bottom: 1.0 * PdfPageFormat.mm, top: 1.0 * PdfPageFormat.mm, left: 2.0 * PdfPageFormat.mm, right: 2.0 * PdfPageFormat.mm),
            padding: const EdgeInsets.only(bottom: 1.0 * PdfPageFormat.mm, top: 1.0 * PdfPageFormat.mm, left: 2.0 * PdfPageFormat.mm, right: 2.0 * PdfPageFormat.mm),
            decoration: const BoxDecoration(
                border:
                    BoxBorder(bottom: true, width: 0.5, color: PdfColors.grey)),
            child: Text('Energy Data Report',
                style: Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      footer: (Context context) {
        return Container(
          width: double.infinity,
          height: 40,
          padding: EdgeInsets.all(10),
            color: PdfColor.fromInt(0xff00a251),
            alignment: Alignment.centerRight,
            child: Text('Page ${context.pageNumber} of ${context.pagesCount}',
                style: Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.white)));
      },
      build: (Context context) => <Widget>[
            Header(
                level: 2,
                margin: EdgeInsets.only(bottom: 0.1 * PdfPageFormat.cm, top: 0.5 * PdfPageFormat.cm, left: 1.0 * PdfPageFormat.cm, right: 1.0 * PdfPageFormat.cm),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      //Text('PDF', textScaleFactor: 1),
                      //Image(PdfImage.jpeg(pdf.document, image: File('/logoj.jpg').readAsBytesSync()))
                      Image(org_logo),
                      Text('ENERGY DATA REPORT', style: TextStyle(font: RalewayBlackTTF, fontSize: 18, color: PdfColor.fromInt(0xff00a251))),
                      Image(etrack_logo)
                    ])),
            Container(
              alignment: Alignment.center,
              child: Text('Energy Monitoring/Management for Businesses', style: TextStyle(font: RalewayMediumTTF, fontSize: 14, color: PdfColor.fromInt(0xff00a251))),
            ),
            SizedBox(height: 5),
            Container(
              alignment: Alignment.center,
              child: Text('w: https://etrack.africa, e: support@etrack.africa', style: TextStyle(font: RalewayMediumTTF, fontSize: 14, color: PdfColor.fromInt(0xff00a251))),
            ),
            SizedBox(height: 10),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(left: 1.0 * PdfPageFormat.cm, right: 1.0 * PdfPageFormat.cm, top:5),
              decoration: BoxDecoration(
                border: BoxBorder(
                  width: 0.5,
                  top: true,
                  bottom: true,
                  color: PdfColor.fromInt(0xff00a251)
                ),
                shape: BoxShape.rectangle,
                borderRadius: 10.0,
                color: PdfColors.white,
              ),
              padding: EdgeInsets.all(10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Text('PROFILE', style: TextStyle(font: RalewayBlackTTF, fontSize: 16, color: PdfColor.fromInt(0xff00a251))),
                      SizedBox(height: 15),
                      Text('Global PFI Limited', style: TextStyle(font: RalewayBoldTTF, fontSize: 16, color: PdfColor.fromInt(0xff000000))),
                      SizedBox(height: 10,),
                      Text('E: info@globalpfi.com', style: TextStyle(font: RalewayRegularTTF, fontSize: 14, color: PdfColor.fromInt(0xff000000))),
                      SizedBox(height: 10,),
                    ]
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Text('USERS', style: TextStyle(font: RalewayBlackTTF, fontSize: 16, color: PdfColor.fromInt(0xff00a251))),
                      SizedBox(height: 15),
                      Text('4', style: TextStyle(font: RalewayLightTTF, fontSize: 50, color: PdfColor.fromInt(0xff000000))),
                    ]
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Text('SPACES', style: TextStyle(font: RalewayBlackTTF, fontSize: 16, color: PdfColor.fromInt(0xff00a251))),
                      SizedBox(height: 15),
                      Text('1', style: TextStyle(font: RalewayLightTTF, fontSize: 50, color: PdfColor.fromInt(0xff000000))),
                    ]
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 10),
                      Text('SITES', style: TextStyle(font: RalewayBlackTTF, fontSize: 16, color: PdfColor.fromInt(0xff00a251))),
                      SizedBox(height: 15),
                      Text('1', style: TextStyle(font: RalewayLightTTF, fontSize: 50, color: PdfColor.fromInt(0xff000000))),
                    ]
                  )
                ]
              )
            ),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(left: 1.0 * PdfPageFormat.cm, right: 1.0 * PdfPageFormat.cm),
              decoration: BoxDecoration(
                border: BoxBorder(
                  width: 0.5,
                  top: true,
                  bottom: true,
                  color: PdfColor.fromInt(0xff00a251)
                ),
                shape: BoxShape.rectangle,
                borderRadius: 10.0,
                color: PdfColors.white,
              ),
              padding: EdgeInsets.all(10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    width: 220,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('OSUN MALL', style: TextStyle(font: RalewayBlackTTF, fontSize: 16, color: PdfColor.fromInt(0xff00a251))),
                        SizedBox(height: 10),
                        Text('FEB 09,2020 - FEB 14,2020', style: TextStyle(font: RalewayRegularTTF, fontSize: 14, color: PdfColor.fromInt(0xff000000))),
                        SizedBox(height: 10),
                        Text('Site: Osun Mall Energy Metering', style: TextStyle(font: RalewayRegularTTF, fontSize: 14, color: PdfColor.fromInt(0xff000000))),
                        SizedBox(height: 5),
                        Text('Device: Justrite Main Load', style: TextStyle(font: RalewayRegularTTF, fontSize: 14, color: PdfColor.fromInt(0xff000000))),
                      ]
                    ),
                  ),
                  Container(
                    width: 200,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('SITE ADDRESS', style: TextStyle(font: RalewayBlackTTF, fontSize: 16, color: PdfColor.fromInt(0xff00a251))),
                        SizedBox(height: 10),
                        Text('Fakunle Area, Osogbo, Osun state', style: TextStyle(font: RalewayLightTTF, fontSize: 14, color: PdfColor.fromInt(0xff000000), lineSpacing: 4)),
                      ]
                    )
                  )
                ]
              )
            ),
            SizedBox(height: 20),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(left: 1.0 * PdfPageFormat.cm, right: 1.0 * PdfPageFormat.cm),
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(chart)
                ]
              ),
            ),
          ]));

      //file = File('example.pdf');
      //file.writeAsBytesSync(pdf.save());

      var genfile = File('example.pdf');
      await genfile.writeAsBytes(pdf.save());
      //return genfile.path;
  }

  //'https://etrack-ems.s3.us-east-2.amazonaws.com/org_logos/production/PHLdbDKGjsNHt7ytALoa22hJVnLAIlPu.png'

  Future<PdfImage> pdfImageFromOrgLogo(PdfDocument pdf, String org_logo) async {
    Uint8List bytes;
    await downloadOrgLogo(org_logo).then((retVal){
      bytes = retVal;
    });
        
    final i = img.decodeImage(File('/org_logo.png').readAsBytesSync());
    final image = PdfImage(
      pdf,
      image: i.data.buffer.asUint8List(),
      width: i.width,
      height: i.height,
    );
    return image;
  }

  Future<PdfImage> pdfImageFromEtrackLogo(PdfDocument pdf, String etrack_logo) async {
    Uint8List bytes;
    await downloadEtrackLogo(etrack_logo).then((retVal){
      bytes = retVal;
    });
        
    final i = img.decodeImage(File('/etrack_logo.png').readAsBytesSync());
    final image = PdfImage(
      pdf,
      image: i.data.buffer.asUint8List(),
      width: i.width,
      height: i.height,
    );
    return image;
  }

  Future<PdfImage> pdfImageFromChart(PdfDocument pdf, String chart) async {
    Uint8List bytes;
    await downloadChart(chart).then((retVal){
      bytes = retVal;
    });
        
    final i = img.decodeImage(File('/chart.png').readAsBytesSync());
    final image = PdfImage(
      pdf,
      image: i.data.buffer.asUint8List(),
      width: i.width,
      height: i.height,
    );
    return image;
  }

  Future<Uint8List> downloadOrgLogo(String org_logo) async {
    HttpClient client = HttpClient();
    var _downloadData = List<int>();
    var fileSave = File('org_logo.png');
    await client.getUrl(Uri.parse(org_logo)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) async {
        await response.listen((d) {
          _downloadData.addAll(d);
          },
            onDone: () {
              fileSave.writeAsBytes(_downloadData);
            }
        );
      }
    );
    img.Image thumbnail;
    try{
      img.Image image = img.decodePng(File('/org_logo.png').readAsBytesSync());
      thumbnail = img.copyResize(image, width: 75);
      File('org_logo.png').writeAsBytesSync(img.encodePng(thumbnail));
    }
    catch(Exception){
      print (Exception.toString());
    }
    return thumbnail.getBytes();
  }

  Future<Uint8List> downloadEtrackLogo(String etrack_logo) async {
    HttpClient client = HttpClient();
    var _downloadData = List<int>();
    var fileSave = File('etrack_logo.png');
    await client.getUrl(Uri.parse(etrack_logo)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) async {
        await response.listen((d) {
          _downloadData.addAll(d);
          },
            onDone: () {
              fileSave.writeAsBytes(_downloadData);
            }
        );
      }
    );
    img.Image thumbnail;
    try{
      img.Image image = img.decodePng(File('/etrack_logo.png').readAsBytesSync());
      thumbnail = img.copyResize(image, width: 95);
      File('etrack_logo.png').writeAsBytesSync(img.encodePng(thumbnail));
    }
    catch(Exception){
      print (Exception.toString());
    }
    return thumbnail.getBytes();
  }

  Future<Uint8List> downloadChart(String chart) async {
    HttpClient client = HttpClient();
    var _downloadData = List<int>();
    var fileSave = File('chart.png');
    await client.getUrl(Uri.parse(chart)).then((HttpClientRequest request) {
      return request.close();
    }).then((HttpClientResponse response) async {
        await response.listen((d) {
          _downloadData.addAll(d);
          },
            onDone: () {
              fileSave.writeAsBytes(_downloadData);
            }
        );
      }
    );
    img.Image thumbnail;
    try{
      img.Image image = img.decodePng(File('/chart.png').readAsBytesSync());
      thumbnail = img.copyResize(image, width: 600);
      File('chart.png').writeAsBytesSync(img.encodePng(thumbnail));
    }
    catch(Exception){
      print (Exception.toString());
    }
    return thumbnail.getBytes();
  }

}

