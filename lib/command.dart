import 'package:args/command_runner.dart';
import 'package:cli/models/models.dart';
import 'package:cli/multiple_pdf_page.dart';
import 'package:cli/single_pdf_page.dart';
import 'dart:io';

import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';

class ReportGenCommand extends Command {
  @override
  final name = 'reportgen';
  @override
  final description = 'Report generator in pdf';

  ReportModel reportModel;

  ReportGenCommand() {
    argParser.addOption('device_name', abbr: 'd', callback: (device_name){
      if(device_name != null){
        print('$device_name');
      }
    });
    argParser.addOption('site_code', abbr: 'c', callback: (site_code){
      if(site_code != null){
        print('$site_code');
      }
    });
    argParser.addOption('space_id', abbr: 's', callback: (space_id){
      if(space_id != null){
        print('$space_id');
      }
    });
    argParser.addOption('org_id', abbr: 'o', callback: (org_id){
      if(org_id != null){
        print('$org_id');
      }
    });
    argParser.addOption('from', abbr: 'f', callback: (from){
      if(from != null){
        print('$from');
      }
    });
    argParser.addOption('to', abbr: 't', callback: (to){
      if(to != null){
        print('$to');
      }
    });
    argParser.addOption('segment', abbr: 'e', callback: (segment){
      if(segment != null){
        print('$segment');
      }
    });
    argParser.addOption('interval', abbr: 'i', callback: (interval){
      if(interval != null){
        print('$interval');
      }
    });
    argParser.addOption('address', abbr: 'a', callback: (address){
      if(address != null){
        print('$address');
      }
    });
    argParser.addOption('serial_number', abbr: 'n', callback: (serial_number){
      if(serial_number != null){
        print('$serial_number');
      }
    });
    argParser.addOption('org_logo', abbr: 'p', callback: (org_logo){
      if(org_logo != null){
        print('$org_logo');
      }
    });
    argParser.addOption('etr_logo', abbr: 'q', callback: (etr_logo){
      if(etr_logo != null){
        print('$etr_logo');
      }
    });
    argParser.addOption('org_info', abbr: 'r', callback: (org_info){
      if(org_info != null){
        print('$org_info');
      }
    });
    argParser.addOption('site_addr', abbr: 'u', callback: (site_addr){
      if(site_addr != null){
        print('$site_addr');
      }
    });
    argParser.addOption('site_name', abbr: 'y', callback: (site_name){
      if(site_name != null){
        print('$site_name');
      }
    });
    argParser.addOption('space_name', abbr: 'x', callback: (space_name){
      if(space_name != null){
        print('$space_name');
      }
    });
  }

  // [run] may also return a Future.
  // @override
  // void run() async {
  //   reportModel = ReportModel(argResults['device_name'], argResults['site_code'], argResults['space_id'], argResults['org_id'],
  //                               argResults['from'], argResults['to'], argResults['segment'], argResults['interval'],
  //                               argResults['address'], argResults['serial_number'], argResults['org_logo'], argResults['etr_logo'],
  //                               argResults['org_info'], argResults['site_addr'], argResults['site_name'], argResults['space_name']
  //                               );
  //   print(reportModel.etrackLogo);
  //   print(reportModel.orgLogo);
  //   print(reportModel.spaceName);
  //   print(reportModel.from);
  //   print(reportModel.to);
  //   print(reportModel.deviceName);
  //   print(reportModel.siteName);
  //   print(reportModel.siteAddr);
  //   //PDF Gen
  //   final Document pdf = Document();

  //   SinglePagePdf singlePagePdf = new SinglePagePdf(pdf);
  //   await singlePagePdf.pdfImageFromOrgLogo(pdf.document, 'https://etrack-ems.s3.us-east-2.amazonaws.com/org_logos/production/V31KLVIiVYJwsuWdje57h29uvuQuuHJ9.png').then((orgLogo) async{
  //     await singlePagePdf.pdfImageFromEtrackLogo(pdf.document, 'https://etrack.africa/assets/images/etrack/etrack-logo-in__green.png').then((etrackLogo) async{
  //       await singlePagePdf.pdfImageFromChart(pdf.document, 'https://quickchart.io/chart?bkg=white&f=png&c=%7B%0D%0A++type%3A+%27bar%27%2C%0D%0A++data%3A+%7B%0D%0A++++labels%3A+%5B%27Feb+9%27%2C+%27Feb+10%27%2C+%27Feb+11%27%2C+%27Feb+12%27%2C+%27Feb+13%27%2C+%27Feb+14%27%5D%2C%0D%0A++++datasets%3A+%5B%7B%0D%0A++++++label%3A+%27Daily+Active+Energy+%28kWh%29%27%2C%0D%0A++++++data%3A+%5B408.81%2C+414.38%2C+488.48%2C+495.58%2C+510.77%2C+558.55%5D%0D%0A++++%7D%5D%0D%0A++%7D%0D%0A%7D').then((chart) async{
  //         await singlePagePdf.generatePDF(orgLogo, etrackLogo, chart);
  //       });
  //     });
  //   });

  //   //https://etrack-ems.s3.us-east-2.amazonaws.com/org_logos/production/V31KLVIiVYJwsuWdje57h29uvuQuuHJ9.png

  //   //https://etrack-ems.s3.us-east-2.amazonaws.com/org_logos/production/PHLdbDKGjsNHt7ytALoa22hJVnLAIlPu.png


  //   //MultiplePagePdf singlePagePdf = new MultiplePagePdf(pdf);
  //   // await singlePagePdf.pdfImageFromOrgLogo(pdf.document, 'https://i.ibb.co/Bjhnt8s/Screenshot-from-2020-02-18-18-26-10.png').then((orgLogo) async{
  //   //   await singlePagePdf.pdfImageFromEtrackLogo(pdf.document, 'https://etrack.africa/assets/images/etrack/etrack-logo-in__green.png').then((etrackLogo) async{
  //   //     await singlePagePdf.pdfImageFromChart(pdf.document, 'https://quickchart.io/chart?bkg=white&c=%7B%0A%20%20type%3A%20%27bar%27%2C%0A%20%20data%3A%20%7B%0A%20%20%20%20labels%3A%20%5B%22Jan%2015%22%2C%20%22Jan%2016%22%2C%20%22Jan%2017%22%2C%20%22Jan%2018%22%2C%20%22Jan%2019%22%2C%20%22Jan%2020%22%2C%20%22Jan%2021%22%2C%20%22Jan%2022%22%2C%20%22Jan%2023%22%2C%20%22Jan%2024%22%5D%2C%0A%20%20%20%20datasets%3A%20%5B%7B%0A%20%20%20%20%20%20label%3A%20%27Total%20Active%20Energy%27%2C%0A%20%20%20%20%20%20data%3A%20%5B353.98%2C%20793.10%2C%201292.12%2C%201392.10%2C%201492.12%2C%201870.24%2C%202298.45%2C%202675.76%2C%203102%2C%203539.8%5D%0A%20%20%20%20%7D%5D%0A%20%20%7D%0A%7D').then((chart) async{
  //   //       await singlePagePdf.pdfImageFromChart2(pdf.document, 'https://quickchart.io/chart?bkg=white&c=%7B%0A%20%20type%3A%20%27bar%27%2C%0A%20%20data%3A%20%7B%0A%20%20%20%20labels%3A%20%5B%22Jan%2015%22%2C%20%22Jan%2016%22%2C%20%22Jan%2017%22%2C%20%22Jan%2018%22%2C%20%22Jan%2019%22%2C%20%22Jan%2020%22%2C%20%22Jan%2021%22%2C%20%22Jan%2022%22%2C%20%22Jan%2023%22%2C%20%22Jan%2024%22%5D%2C%0A%20%20%20%20datasets%3A%20%5B%7B%0A%20%20%20%20%20%20label%3A%20%27Total%20Reactive%20Energy%27%2C%0A%20%20%20%20%20%20data%3A%20%5B65.35%2C%20130.7%2C%20201.12%2C%20234.10%2C%20267.11%2C%20332.41%2C%20392.67%2C%20466.71%2C%20545.56%2C%20630.36%5D%0A%20%20%20%20%7D%5D%0A%20%20%7D%0A%7D').then((chart2) async{
  //   //         await singlePagePdf.pdfImageFromChart3(pdf.document, 'https://quickchart.io/chart?bkg=white&c=%7B%0A%20%20type%3A%20%27bar%27%2C%0A%20%20data%3A%20%7B%0A%20%20%20%20labels%3A%20%5B%22Jan%2015%22%2C%20%22Jan%2016%22%2C%20%22Jan%2017%22%2C%20%22Jan%2018%22%2C%20%22Jan%2019%22%2C%20%22Jan%2020%22%2C%20%22Jan%2021%22%2C%20%22Jan%2022%22%2C%20%22Jan%2023%22%2C%20%22Jan%2024%22%5D%2C%0A%20%20%20%20datasets%3A%20%5B%7B%0A%20%20%20%20%20%20label%3A%20%27Total%20Active%20Energy%27%2C%0A%20%20%20%20%20%20data%3A%20%5B353.98%2C%20793.10%2C%201292.12%2C%201392.10%2C%201492.12%2C%201870.24%2C%202298.45%2C%202675.76%2C%203102%2C%203539.8%5D%0A%20%20%20%20%7D%2C%7B%0A%20%20%20%20%20%20label%3A%20%27Total%20Reactive%20Energy%27%2C%0A%20%20%20%20%20%20data%3A%20%5B65.35%2C%20130.7%2C%20201.12%2C%20234.10%2C%20267.11%2C%20332.41%2C%20392.67%2C%20466.71%2C%20545.56%2C%20630.36%5D%0A%20%20%20%20%7D%5D%0A%20%20%7D%0A%7D').then((chart3) async{
  //   //           await singlePagePdf.generatePDF(orgLogo, etrackLogo, chart, chart2, chart3);
  //   //         });
  //   //       });
  //   //     });
  //   //   });
  //   // });
  // }
}

//https://quickchart.io/chart?bkg=white&f=png&w=600&h=400&c=%7B%0D%0A++type%3A+%27bar%27%2C%0D%0A++data%3A+%7B%0D%0A++++labels%3A+%5B%27Feb+9%27%2C+%27Feb+10%27%2C+%27Feb+11%27%2C+%27Feb+12%27%2C+%27Feb+13%27%2C+%27Feb+14%27%5D%2C%0D%0A++++datasets%3A+%5B%7B%0D%0A++++++label%3A+%27Daily+Active+Energy+%28kWh%29%27%2C%0D%0A++++++data%3A+%5B408.81%2C+414.38%2C+488.48%2C+495.58%2C+510.77%2C+558.55%5D%0D%0A++++%7D%5D%0D%0A++%7D%0D%0A%7D

//https://quickchart.io/chart?bkg=white&c=%7B%0A%20%20type%3A%20%27bar%27%2C%0A%20%20data%3A%20%7B%0A%20%20%20%20labels%3A%20%5B2000%2C%202001%2C%202002%2C%202003%2C%202004%2C%202005%2C%202006%2C%202007%2C%202008%2C%202009%2C%202010%2C%202011%2C%202012%2C%202013%2C%202014%2C%202015%2C%202016%2C%202017%2C%202018%2C%202019%2C%202020%2C%202021%2C%202022%2C%202023%2C%202024%2C%202025%2C%202026%2C%202027%2C%202028%2C%202029%2C%202030%2C%202031%5D%2C%0A%20%20%20%20datasets%3A%20%5B%7B%0A%20%20%20%20%20%20label%3A%20%27Raisins%27%2C%0A%20%20%20%20%20%20data%3A%20%5B12%2C%206%2C%205%2C%2018%2C%2012%2C%204%2C%205%2C%207%2C%208%2C%209%2C%2010%2C%2011%2C%2014%2C%2015%2C%208%2C%209%2C%205%2C%204%2C%209%2C%2013%2C%206%2C%204%2C%205%2C%206%2C%209%2C%2010%2C%2011%2C%2018%2C%2019%2C%2010%2C%2011%5D%0A%20%20%20%20%7D%5D%0A%20%20%7D%0A%7D