import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;

enum Permissions {
  private,
  public,
}

class AWSFileUpload{
  String orgName;
  String region;
  String accessKey;
  String secretKey;
  String service;
  String endpointUrl;
  int dateTime;
  HttpClient httpClient = new HttpClient();

  AWSFileUpload(String region, String accessKey, String secretKey, String service, String endpointUrl, int dateTime, String orgName){
    this.region = region;
    this.accessKey = accessKey;
    this.secretKey = secretKey;
    this.service = service;
    this.endpointUrl = endpointUrl;
    this.dateTime = dateTime;
    this.orgName = orgName;
  }

  String _uriEncode(String str) {
    return Uri.encodeQueryComponent(str).replaceAll('+', '%20');
  }

  String _trimAll(String str) {
    String res = str.trim();
    int len;
    do {
      len = res.length;
      res = res.replaceAll('  ', ' ');
    } while (res.length != len);
    return res;
  }

  String getCoolPDFName(String name, int dateTime){
    var d = DateTime.fromMillisecondsSinceEpoch(dateTime);
    return ('${name}'+'_'+'${d.day}'+'${d.month}'+'${d.year}'+'${d.hour}'+'${d.minute}'+'${d.second}');
  }

  String signRequest(http.BaseRequest request,
      {Digest contentSha256, bool preSignedUrl = false, int expires = 86400}) {
    // Build canonical request
    DateTime date;
    Map<String, String> headers, queryParameters;
    List<String> headerNames, queryKeys;
    String httpMethod, canonicalURI, dateIso8601, dateYYYYMMDD, hashedPayloadStr, 
    credential, canonicalHeaders, signedHeaders, canonicalRequest, stringToSign;
    Digest canonicalRequestHash,dateKey, dateRegionKey, dateRegionServiceKey, signingKey, signature;

    httpMethod = request.method;
    canonicalURI = '/reports/${getCoolPDFName(orgName, dateTime)}.pdf';
    //String canonicalURI = request.url.path;
    //String host = request.url.toString();
    //String _service = service;

    
    date = DateTime.now().toUtc();
    
    dateIso8601 = date.toIso8601String();
    dateIso8601 = dateIso8601
            .substring(0, dateIso8601.indexOf('.'))
            .replaceAll(':', '')
            .replaceAll('-', '') +
        'Z';
    dateYYYYMMDD = date.year.toString().padLeft(4, '0') +
        date.month.toString().padLeft(2, '0') +
        date.day.toString().padLeft(2, '0');

    hashedPayloadStr = contentSha256 == null ? 'UNSIGNED-PAYLOAD' : '$contentSha256';

    credential = '${accessKey}/${dateYYYYMMDD}/${region}/${service}/aws4_request';

    // Build canonical headers string
    headers = Map<String, String>();
    if (!preSignedUrl) {
      request.headers['x-amz-date'] = dateIso8601; // Set date in header
      if (contentSha256 != null) {
        request.headers['x-amz-content-sha256'] = hashedPayloadStr; // Set payload hash in header
      }
      request.headers.keys.forEach((String name) =>
          (headers[name.toLowerCase()] = request.headers[name]));
    }
    //headers['host'] = 'inventech-etrack.s3.us-east-2.amazonaws.com'; // Host is a builtin header
    headerNames = headers.keys.toList()..sort();
    canonicalHeaders = headerNames.map((s) => '${s}:${_trimAll(headers[s])}' + '\n').join();
    signedHeaders = 'host;x-amz-content-sha256;x-amz-date';

    // Build canonical query string
    queryParameters = Map<String, String>()..addAll(request.url.queryParameters);
    if (preSignedUrl) {
      queryParameters['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
      queryParameters['X-Amz-Credential'] = credential;
      queryParameters['X-Amz-Date'] = dateIso8601;
      queryParameters['X-Amz-Expires'] = expires.toString();
      if (contentSha256 != null) {
        queryParameters['X-Amz-Content-Sha256'] = hashedPayloadStr;
      }
      queryParameters['X-Amz-SignedHeaders'] = signedHeaders;
    }
    queryKeys = queryParameters.keys.toList()..sort();
    String canonicalQueryString;
    canonicalQueryString = queryKeys
        .map((s) => '${_uriEncode(s)}=${_uriEncode(queryParameters[s])}')
        .join('&');

    if (preSignedUrl) {
      hashedPayloadStr = 'UNSIGNED-PAYLOAD';
    }
    
    // Sign headers
    canonicalRequest = '${httpMethod}\n${canonicalURI}\n${canonicalQueryString}\n${canonicalHeaders}\n${signedHeaders}\n$hashedPayloadStr';
    //print('\n>>>>>> canonical request \n' + canonicalRequest + '\n<<<<<<\n');

    canonicalRequestHash = sha256.convert(canonicalRequest.trim().codeUnits);
    
    stringToSign = 'AWS4-HMAC-SHA256\n${dateIso8601}\n${dateYYYYMMDD}/us-east-2/s3/aws4_request\n$canonicalRequestHash';
    //print('\n>>>>>> string to sign \n' + stringToSign + '\n<<<<<<\n');
    
    dateKey = Hmac(sha256, 'AWS4${secretKey}'.codeUnits).convert(dateYYYYMMDD.trim().codeUnits);
    dateRegionKey = Hmac(sha256, dateKey.bytes).convert(region.trim().codeUnits);
    dateRegionServiceKey = Hmac(sha256, dateRegionKey.bytes).convert(service.trim().codeUnits);
    signingKey = Hmac(sha256, dateRegionServiceKey.bytes).convert('aws4_request'.trim().codeUnits);
    signature = Hmac(sha256, signingKey.bytes).convert(stringToSign.trim().codeUnits);

    // Set signature in header
    request.headers['Authorization'] = 'AWS4-HMAC-SHA256 Credential=${credential}, SignedHeaders=${signedHeaders}, Signature=$signature';
    
    //print(request.headers);

    if (preSignedUrl) {
      queryParameters['X-Amz-Signature'] = '$signature';
      return request.url.replace(queryParameters: queryParameters).toString();
    } else {
      return null;
    }
  }

  Future<String> uploadFile(
      String key, File file, String contentType, Permissions permissions,
      {Map<String, String> meta}) async {
    var _downloadData;
    //int contentLength;
    Digest contentSha256;
    String uriStr;
    int sCode;
    http.StreamedRequest request;
    Stream<List<int>> stream;

    _downloadData = List<int>();
    //contentLength = await file.length();
    contentSha256 = await sha256.bind(file.openRead()).first;
    uriStr = endpointUrl + key;
    request = await http.StreamedRequest('PUT', Uri.parse(uriStr));
    stream = await file.openRead();
    try{
      await stream.listen((d){
        _downloadData.addAll(d);
        request.sink.add(d);
      },
      onError: request.sink.addError, onDone: request.sink.close).asFuture().whenComplete(() async{
        request.headers['Host'] = 'inventech-etrack.s3.us-east-2.amazonaws.com';
        //request.headers['Content-Length'] = contentLength.toString();
        //request.headers['Content-Type'] = contentType;
        // request.headers['AWSAccessKeyId'] = 'AKIAI6UBNKMUUUG5AMHQ';
        // request.headers['AWSSecretAccessKey'] = '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';
        signRequest(request, contentSha256: contentSha256);
        await http.Client().put(request.url, headers: request.headers, body: _downloadData).then((response) async{
          //String body = await utf8.decodeStream(response.stream);

          /***Uncomment this line for Debugging***/
          if (response.statusCode != 200) {
            throw ClientException(response.statusCode, response.reasonPhrase, response.headers, response.body);
          }
          sCode = response.statusCode;
          return (sCode.toString());
        });
      });
    }
    catch(err){
      print('$err');
      return sCode.toString();
    }
    finally{
      return sCode.toString();
    }
    // if (meta != null) {
    //   for (MapEntry<String, String> me in meta.entries) {
    //     request.headers["x-amz-meta-${me.key}"] = me.value;
    //   }
    // }
    // if (permissions == Permissions.public) {
    //   request.headers['x-amz-acl'] = 'public-read';
    // }
    
  }
}

class ClientException implements Exception {
  final int statusCode;
  final String reasonPhrase;
  final Map<String, String> responseHeaders;
  final String responseBody;
  const ClientException(this.statusCode, this.reasonPhrase,
      this.responseHeaders, this.responseBody);
  @override
  String toString() {
    return 'DOException { statusCode: ${statusCode}, reasonPhrase: \"${reasonPhrase}\", responseBody: \"${responseBody}\" }';
  }
}