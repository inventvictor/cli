class ReportModel{
  DocTypeModel docTypeModel;
  OrgModel org;
  SiteModel site;
  int from;
  int to;
  String spaceName;
  InvoiceModel invoiceModel;
  GraphModel graph;

  ReportModel(DocTypeModel docTypeModel, OrgModel org, SiteModel site, int from, int to, String spaceName, InvoiceModel invoiceModel, GraphModel graph){
    this.docTypeModel = docTypeModel;
    this.org = org;
    this.site = site;
    this.from = from;
    this.to = to;
    this.spaceName = spaceName;
    this.invoiceModel = invoiceModel;
    this.graph = graph;
  }

  ReportModel.fromJson(Map<dynamic, dynamic> json)
      : org = json['org'],
        site = json['site'],
        from = json['from'],
        to = json['to'],
        spaceName = json['space_name'],
        graph = json['graphs'];

  Map<dynamic, dynamic> toJson() => {
        'org': org,
        'site': site,
        'from': from,
        'to': to,
        'space_name': spaceName,
        'graphs': graph
      };
}

class DocTypeModel{
  String doc_type;

  DocTypeModel(String doc_type) {
    this.doc_type = doc_type;
  }

  DocTypeModel.fromJson(Map<dynamic, dynamic> json)
      : doc_type = json['doc_type'];

  Map<dynamic, dynamic> toJson() => {
        'doc_type': doc_type
      };
}

class OrgModel{
  String logo;
  String name;
  String email;
  Map<String, dynamic> n;

  OrgModel(String logo, String name, String email, Map<String, dynamic> n){
    this.logo = logo;
    this.name = name;
    this.email = email;
    this.n = n;
  }

  OrgModel.fromJson(Map<dynamic, dynamic> json)
      : logo = json['org']['logo'],
        name = json['org']['name'],
        email = json['org']['email'],
        n = json['org']['n'];

  Map<dynamic, dynamic> toJson() => {
        'org': {
          'logo': logo,
          'name': name,
          'email': email,
          'n': n
        }
      };
  
}

class OrgNModel{
  int users;
  int spaces;
  int sites;

  OrgNModel(int users, int spaces, int sites){
    this.users = users;
    this.spaces = spaces;
    this.sites = sites;
  }

  OrgNModel.fromJson(Map<dynamic, dynamic> json)
      : users = json['users'],
        spaces = json['spaces'],
        sites = json['sites'];

  Map<dynamic, dynamic> toJson() => {
        'n': {
          'users': users,
          'spaces': spaces,
          'sites': sites
        }
      };
}

class SiteModel{
  String name;
  String addr;
  String device;
  String deviceType;

  SiteModel(String name, String addr, String device, String deviceType){
    this.name = name;
    this.addr = addr;
    this.device = device;
    this.deviceType = deviceType;
  }

  SiteModel.fromJson(Map<dynamic, dynamic> json)
      : name = json['site']['name'],
        addr = json['site']['addr'],
        device = json['site']['device'],
        deviceType = json['site']['device_type'];

  Map<dynamic, dynamic> toJson() => {
        'site': {
          'name': name,
          'addr': addr,
          'device': device,
          'device_type': deviceType
        }
      };
}

class InvoiceModel{
  double energyUsed;
  double averagePeak;
  String unit;
  double value;
  double tariff;
  String curr;
  double extraCharges;

  InvoiceModel(double energyUsed, double averagePeak, String unit, double value, double tariff, String curr, double extraCharges){
    this.energyUsed = energyUsed;
    this.averagePeak = averagePeak;
    this.unit = unit;
    this.value = value;
    this.tariff = tariff;
    this.curr = curr;
    this.extraCharges = extraCharges;
  }

  InvoiceModel.fromJson(Map<dynamic, dynamic> json)
      : energyUsed = json['invoice_data']['energy_used'],
        averagePeak = json['invoice_data']['average_peak'],
        unit = json['invoice_data']['unit'],
        value = json['invoice_data']['value'],
        tariff = json['invoice_data']['tariff'],
        curr = json['invoice_data']['curr'],
        extraCharges = json['invoice_data']['extra_charges'];

  Map<dynamic, dynamic> toJson() => {
        'invoice_data': {
          'energy_used': energyUsed,
          'average_peak': averagePeak,
          'unit': unit,
          'value': value,
          'tariff': tariff,
          'curr': curr,
          'extra_charges': extraCharges
        }
      };

}

class GraphModel{
  List<dynamic> data;

  GraphModel(List<dynamic> data){
    this.data = data;
  }

  GraphModel.fromJson(Map<dynamic, dynamic> json)
      : data = json['graphs'];

  Map<dynamic, dynamic> toJson() => {
        'graphs': data
      };
}

class GraphDataModel{
  String label;
  List<dynamic> x;
  List<dynamic> y;
  Map<String, dynamic> compareTo;

  GraphDataModel(String label, List<dynamic> x, List<dynamic> y, Map<String, dynamic> compareTo){
    this.label = label;
    this.x = x;
    this.y = y;
    this.compareTo = compareTo;
  }

  GraphDataModel.fromJson(Map<dynamic, dynamic> json)
      : label = json['data']['label'],
        x = json['data']['x'],
        y = json['data']['y'],
        compareTo = json['data']['compare_to'];

  Map<dynamic, dynamic> toJson() => {
        'data': {
          'label': label,
          'x': x,
          'y': y,
          'compare_to':compareTo
        }
      };
}

class GraphCompareToModel{
  String label;
  List<dynamic> y;

  GraphCompareToModel(String label, List<dynamic> y){
    this.label = label;
    this.y = y;
  }

  GraphCompareToModel.fromJson(Map<dynamic, dynamic> json)
      : label = json['label'],
        y = json['y'];

  Map<dynamic, dynamic> toJson() => {
        'compare_to': {
          'label': label,
          'y': y
        }
      };
}