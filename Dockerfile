#FROM google/dart 
#FROM google/dart-runtime

FROM google/dart AS dart-runtime

WORKDIR /app

ADD pubspec.* /app/
RUN pub get
ADD . /app
#ADD assets /assets/
#ADD bin /app/bin/
#ADD lib /app/lib/

RUN pub get --offline
RUN dart2native /app/bin/main.dart  -k aot

FROM bitnami/minideb

COPY --from=dart-runtime /app/bin/main.aot /main.aot
COPY --from=dart-runtime /usr/lib/dart/bin/dartaotruntime /dartaotruntime
COPY --from=dart-runtime /app/assets /assets
COPY --from=dart-runtime /app/.env .env

CMD []
#ENTRYPOINT ["/usr/bin/dart", "/app/bin/main.dart"]
ENTRYPOINT ["/dartaotruntime", "/main.aot"]

EXPOSE 8080